
{config, pkgs, ...}:

{  
   nixpkgs.config.allowUnfree = true;
   
   imports = [
      ./minimal-ui.nix
      ./services/printer.nix
      ./modules/calyxos
   ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
   users.users.vic.packages = with pkgs; [
      logseq
      transmission_3
      signal-desktop
      qtox
      vlc
      
      gcc
      gnumake
      cmake
      pkg-config
      binutils
      valgrind
      gdb
      boost185
      boost185.dev
      capnproto
      
      spotify
      spotify-tray
      
      php
      wineWowPackages.minimal
      nmap
      qFlipper
      python311Packages.pip
      usbutils
      nodejs

      cargo
      rustc

      openjdk
      maven
      gradle
      
      dbeaver-bin

      #teamviewer
      rar
      #woeusb
      android-studio
      yuicompressor
      uglify-js
      html-minifier
      terser
   ];

   services.mysql = {
      enable = true;
      package = pkgs.mariadb;
   };

   users.users.vic.extraGroups = [ "dialout" "wheel" "adbuser" "kvm" ];
   
   virtualisation.docker.enable = true;
   programs.adb.enable = true;
#   services.teamviewer.enable = true;
}
