{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.autoLuksUnlock;
in {

  options.services.autoLuksUnlock = {
    enable = mkEnableOption "Automatic LUKS decryption at login";

    encryptedDevice = mkOption {
      type = types.str;
      default = "/dev/sdX1";
      description = "The device path of the encrypted LUKS partition.";
    };

    unlockedDeviceName = mkOption {
      type = types.str;
      default = "unlockedPartition";
      description = "The name to assign to the unlocked LUKS partition.";
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.cryptsetup ];

    security.pam.services = {
      login.postAuth = [
        {
          type = "session";
          control = "optional";
          module = "${pkgs.pam}/lib/security/pam_exec.so";
          arguments = [ "quiet" cfg.scriptPath ];
        }
      ];
    };

    # Generate the unlock script
    systemd.tmpfiles.rules = [
      "C ${config.system.build.toplevel}/bin/auto-luks-unlock 0755 root root -"
    ];

    environment.etc."auto-luks-unlock" = {
      mode = "0755";
      text = ''
        #!/bin/sh
        if [ "$PAM_TYPE" != "open_session" ]; then
          return
        fi
        read password
        echo $password | cryptsetup luksOpen ${cfg.encryptedDevice} ${cfg.unlockedDeviceName} --key-file=-
      '';
    };
  };
}

