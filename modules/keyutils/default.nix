{ config, pkgs, ... }:
 let 
      pythonEnv = pkgs.python3.withPackages (ps: with ps; [
         mnemonic
      ]);

in
{
  imports = [
    ../../modules/fetch-git-scripts
  ];

  services.fetchGitScript = {
    enable = true;
    url = "https://gitlab.com/ficul/keyutils.git";
    rev = "v0.0.4";
    sha256 = "sha256-jzywf5naBmSJyldjs0uXx16kkHh8Fejb38hThKPFk34=";
    scriptPaths = ["seed2bip39.py" "randombip39.sh" "words2bip39.sh" "words2seed.py" "words2index.py" "index2words.py" "index2letters.py" "letters2index.py"];
    dependencies = [ pythonEnv ];
  };

  # Your other configurations...
}
