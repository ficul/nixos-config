{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.fetchGitScript;
in
{
  options.services.fetchGitScript = {
    enable = mkEnableOption "Fetch script from GitHub";

    url = mkOption {
      type = types.str;
      default = "";
      description = "Git repository.";
    };


    rev = mkOption {
      type = types.str;
      default = "";
      description = "Git revision such as a commit hash or branch name.";
    };

    sha256 = mkOption {
      type = types.str;
      default = "";
      description = "SHA256 hash of the repository contents.";
    };

    #scriptPath = mkOption {
    #  type = types.str;
    #  default = "";
    #  description = "Path to the script within the repository.";
    #};

    scriptPaths = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "Paths to the scripts within the repository.";
    };
    

    dependencies = mkOption {
      type = types.listOf types.package;
      default = [];
      example = literalExample "[ pkgs.jq pkgs.curl ]";
      description = "List of Nix package dependencies required by the script.";
    };
  };

 # config = mkIf cfg.enable {
 #   environment.systemPackages = with pkgs; [
 #     (let
 #       scriptSrc = fetchgit {
 #         url = cfg.url;
 #         rev = cfg.rev;
 #         sha256 = cfg.sha256;
 #       };
 #       scriptPkg = stdenv.mkDerivation {
 #         name = "git-script";
 #         src = scriptSrc;
 #
 #         installPhase = ''
 #           mkdir -p $out/bin
 #           cp ${scriptSrc}/${cfg.scriptPath} $out/bin/${cfg.scriptPath}
 #           chmod +x $out/bin/${cfg.scriptPath}
 #         '';
 #       };
 #     in scriptPkg)
 #   ] ++ cfg.dependencies;
 # };



 config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      # Map each scriptPath to a package
    ] ++ (map (scriptPath: pkgs.stdenv.mkDerivation {
      name = "git-script-${baseNameOf scriptPath}";

      src = pkgs.fetchgit {
        url = cfg.url;
        rev = cfg.rev;
        sha256 = cfg.sha256;
      };

      buildInputs = cfg.dependencies;

      dontBuild = true;

      installPhase = ''
        mkdir -p $out/bin
        cp $src/${scriptPath} $out/bin/${baseNameOf scriptPath}
        chmod +x $out/bin/${baseNameOf scriptPath}
      '';
    }) cfg.scriptPaths) ++ cfg.dependencies;
  };

 
 
}

