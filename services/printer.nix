{pkgs, ...} : {


	#Enable CUPS printing service
	services.printing = {
	  enable = true;
	  logLevel = "debug";
	  drivers = [ pkgs.gutenprint ];
	};
}
